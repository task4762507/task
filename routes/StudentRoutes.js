const express=require('express')
const { PostStudent } = require('../controller/Methods')
const { PostCollege, PopulateStudent } = require('../controller/College')
const { PostMarks, PopulateMarks, AggereateMarks } = require('../controller/MarksDetails')

const router=express.Router()

// Student Details
router.post('/postStudent',PostStudent)

//College Details
router.post('/StudentCollege',PostCollege)

router.get('/populateCollege',PopulateStudent)

//Marks Details

router.post('/PostMarks',PostMarks)

router.get('/populateMarks',PopulateMarks)

router.get('/aggregateDetails',AggereateMarks)


module.exports=router;