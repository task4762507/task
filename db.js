const mongoose=require('mongoose')
const { StudnetSchema } = require('./Schema/Student')
const { CollegeSchema } = require('./Schema/College')
const { MarksSchema } = require('./Schema/Marks')

const student=new mongoose.model('student',StudnetSchema)

const college=new mongoose.model('college',CollegeSchema)

const marks=new mongoose.model('marks',MarksSchema)

module.exports={
    student,
    college,
    marks
}