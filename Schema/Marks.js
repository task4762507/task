const mongoose=require('mongoose')

const MarksSchema=new mongoose.Schema({
    id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"student"
    },
    subject:{
        type:String
    },
    marks:{
        type:Number
    }
},
{
    timestamps:true
}
)

module.exports={
    MarksSchema
}