const mongoose=require('mongoose')

const StudnetSchema=new mongoose.Schema({

    id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'college'
    },
    firstname:{
        type:String,
        lowercase:true
    },
    lastname:{
        type:String,
        lowercase:true
    },
    email:{
        type:String,
        lowercase:true
    }
},{
    timestamps:true
}
)


module.exports={
    StudnetSchema
}