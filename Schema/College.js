const mongoose=require('mongoose')

const CollegeSchema=new mongoose.Schema({
   
    CollegeName:{
        type:String
    },
    CourseName:{
        type:String
    }

},
{
    timestamps:true
}
)

module.exports={
    CollegeSchema
}