const { college } = require("../db")

module.exports.PostCollege=async(req,res)=>{
    try{

    
    const collegeName=req.body.collegeName
    const course=req.body.course

    // const exist=await college.findOne({id:id})
    // if(exist){
    //     return res.status(400).json({
    //         msg:"user already exist"
    //     })
       
    // }
    

    const collegeCreate=await college.create({
       
        CollegeName:collegeName,
        CourseName:course

    })
    return res.status(200).json({
        collegeCreate
    })

    }catch(error){
        return res.status(400).json({
            msg:error.message
        })
    }
}

module.exports.PopulateStudent=async(req,res)=>{

    const id=req.query.id

    const user=await college.findOne({id:id}).populate('id')

    return res.status(200).json({
        user
    })
}