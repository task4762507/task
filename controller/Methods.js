const { student } = require("../db")
const { StudentValidation } = require("../validation/Validation")


module.exports.PostStudent=async(req,res)=>{

   try{
    // const validate=await StudentValidation.validateAsync(req.body)
    const id=req.query.id
    const firstname=req.body.firstname
    const lastname=req.body.lastname
    const email=req.body.email

    const exist= await student.findOne({email:email})
    // console.log(exist)
    if(exist){
        return res.status(400).json({
            msg:'user already exist'
        })

        
    }

   const user= await student.create({
        id:id,
        firstname:firstname,
        lastname:lastname,
        email:email
    })

    return res.status(200).json({
        user
    })


   }catch(error){
    return res.status(400).json({
        msg:error.message
    })
   }   
}