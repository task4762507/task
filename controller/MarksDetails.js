const { marks } = require("../db")
const mongoose=require('mongoose')
const mongodb=require('mongodb')
module.exports.PostMarks=async(req,res)=>{
    
    try{
        const id=req.query.id
        const subject=req.body.subject
        const Marks=req.body.marks

        const marksCreated= await marks.create({
            id:id,
            subject:subject,
            marks:Marks
        })

        return res.status(200).json({
            marksCreated
        })

        
    }catch(error){
        return res.status(400).json({
            msg:error.message
        })
    }
}

module.exports.PopulateMarks=async(req,res)=>{
    try{
        const id=req.query.id;
        const marksPopulated=await marks.find({id:id}).populate({
            path:'id',
            populate:{

                path:'id',
                model:'college',
                select:'CollegeName'
            }
        })

        return res.status(200).json({
            marksPopulated
        })

    }catch(error){
        return res.status(400).json({
            msg:error.message
        })
    }
}

module.exports.AggereateMarks=async(req,res)=>{

    try{
        let id=req.query.id
        // const ObjectId =  mongoose.Types.ObjectId;
        // id =  new ObjectId;
        // const objectId1 = mongoose.Types.ObjectId(id);
        // const ObjectId=new mongoose.ObjectId(id)
        // console.log(objectId1)
        // id=mongoose.Types.ObjectId('4edd40c86762e0fb12000003');
       
        // console.log(Mongoose.Types.ObjectId(id))
        // const user=await marks.find({'$id':id})
        const objectId = new mongodb.ObjectId(id)
        console.log(objectId)
        const DB=await marks.aggregate([
            {
                $match:{
                    id:objectId
                }
            },
            {
                $sort:{createdAt:-1}
            },
             {
                $lookup:{
                    from:'students',
                    localField:'id',
                    foreignField:'_id',
                    as:'NewArray'

                    
                }
             },

             {
               $lookup:{
                from:"colleges",
                localField:'NewArray.id',
                foreignField:"_id",
                as:"Output"
               }
             },
            
             {
                $unwind:"$NewArray"
                
             },
             {
                $unwind:"$Output"
                
             },
             {
                $project:{
                    firstname:"$NewArray.firstname",
                    lastname:"$NewArray.lastname",
                    collegeName:'$Output.CollegeName',
                    marks:1,
                    subject:1}
             },
             {
                $limit:5
             }
             
            ])
            // console.log(DB.NewArray)

        return res.status(200).json({
            DB
        })

    }catch(error){
        return res.status(400).json({
            msg:error.message
        })
    }
}