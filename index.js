const express=require('express')
const { mongoose_Connect } = require('./config')
const MainRouter=require('./routes/StudentRoutes')

const app=express()

app.use(express.json())

app.use('/api/v1',MainRouter)


app.listen(3000,()=>{
    mongoose_Connect
})