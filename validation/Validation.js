const joi=require('joi')

const StudentValidation=joi.object({
    firstname:joi.string().required(),
    lastname:joi.string().required(),
    email:joi.string().required()
    
})

module.exports={
    StudentValidation
}